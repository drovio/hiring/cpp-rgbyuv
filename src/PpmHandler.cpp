#include "PpmHandler.h"

#include <fstream>

PpmHandler::PpmHandler() : PpmHandler(1, 1, 255) {
}

PpmHandler::PpmHandler(unsigned int width, unsigned int height, unsigned int color_levels) : PnmBase(width, height, color_levels) {
  InitBuffer("P6", width, height, color_levels);
}

PpmHandler::~PpmHandler() {
}

bool PpmHandler::Read(const std::string& filename) {
  std::ifstream file(filename.c_str(), std::ios_base::binary);
  
  if (!file.is_open()) {
    return false;
  }

  if (InitBufferFromFile(&file) &&
      signature() != "P6" ) {
    return false;
  }

  for(int i = 0; i < width() * height(); i++) {
    unsigned char sc[3];
    file.read(reinterpret_cast<char*>(sc), 3);
    *(data() + i) = 0x00000000 | ((sc[0] << 16) | (sc[1] << 8) | sc[2]);
  }

  file.close();

  return true;
}

bool PpmHandler::Write(const std::string& filename) {
	std::ofstream file(filename.c_str(), std::ios_base::binary);
  
  if (!file.is_open()) {
    return false;
  }

	std::ostringstream os_width;
	os_width << width();

	std::ostringstream os_height;
	os_height << height();

	std::ostringstream os_clevels;
	os_clevels << color_levels();

	std::string sline = "\n";
	std::string swidth = os_width.str();
	std::string sheight = os_height.str();
	std::string sclevels = os_clevels.str();

	file.write("P6", 2);
	file.write(sline.data(), sline.size());
	file.write(swidth.data(), swidth.size());
	file.write(" ", 1);
	file.write(sheight.data(), sheight.size());
	file.write(sline.data(), sline.size());
	file.write(sclevels.data(), 3);
	file.write(sline.data(), sline.size());

	for(int i = 0; i < width() * height(); i++) {
		char sc[3];

		for(int j = 0; j < 3; j++) {
			sc[j] = static_cast<char> ((*(data() + i) & (0x00FF0000 >> j*8)) >> ((2-j)*8));
		}

		file.write(sc, 3);
	}

	file.close();

	return true;
}
