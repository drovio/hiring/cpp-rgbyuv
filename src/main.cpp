#include <iostream>

#include "Config.h"
#include "PpmHandler.h"

int main(int argc, char* argv[]) {
  if (argc < 2) {
    std::cout << argv[0] << " version " << VERSION_MAJOR << "."
              << VERSION_MINOR << std::endl;
    std::cout << "Usage: " << argv[0] << " image.ppm" << std::endl;
    return 1;
  }
  
  PpmHandler image;
  
  if(!image.Read(argv[1])) {
    std::cout << "Can't find image filename " << argv[1] << std::endl;
    return 1;
  }

  return 0;
}
