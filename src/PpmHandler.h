#ifndef __PPMHANDLER_H__
#define __PPMHANDLER_H__

#include "PnmBase.h"

class PpmHandler : public PnmBase<unsigned int> {
 public:
	PpmHandler();
	PpmHandler(unsigned int width, unsigned int height, unsigned int color_levels = 255);
  
  virtual ~PpmHandler();
  
  virtual bool Read(const std::string& filename);
	
  virtual bool Write(const std::string& filename);
};

#endif
